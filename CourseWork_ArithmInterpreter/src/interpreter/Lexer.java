package interpreter;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lexer {

    private ArrayList<Token> tokens;
    private Map<String, String> terminals;
    private Map<String, String> keywords;
    private Pattern CommonPattern;
    private String path;
    private BufferedReader reader;

    private String VAR = "[A-Za-z_]+\\w*";
    private String NUM = "0|([1-9][0-9]*)";
    private String OP = "\\+|-|\\/|\\*|\\||&|(<=)|(>=)|(==)|(<>)|>|<";
    private String ASSIGN_OP = "=";
    private String LPAR = "\\(";
    private String RPAR = "\\)";
    private String BEGIN_KW = "begin";
    private String END_KW = "end";
    private String IF_KW = "if";
    private String ELSE_KW = "else";
    private String SM = ";";
    private String WHILE_KW = "while";
    private String VAR_KW = "var";
    private String FOR_KW = "for";
    private String DO_KW = "do";
    private String WRITE_KW = "write";
    private String WRITE_TABLE_KW = "write_table";

    public Lexer(String path) throws FileNotFoundException {
        tokens = new ArrayList(100);

        terminals = new HashMap<>();
        keywords = new HashMap<>();
        terminals.put("VAR", VAR);
        terminals.put("NUM", NUM);
        terminals.put("OP", OP);
        terminals.put("ASSIGNOP", ASSIGN_OP);
        terminals.put("LPAR", LPAR);
        terminals.put("RPAR", RPAR);
        terminals.put("SM", SM);
        keywords.put("IFKW", IF_KW);
        keywords.put("ELSEKW", ELSE_KW);
        keywords.put("WHILEKW", WHILE_KW);
        keywords.put("VARKW", VAR_KW);
        keywords.put("FORKW", FOR_KW);
        keywords.put("BEGINKW", BEGIN_KW);
        keywords.put("ENDKW", END_KW);
        keywords.put("DOKW", DO_KW);
        keywords.put("WRITEKW", WRITE_KW);
        keywords.put("WRITETABLEKW", WRITE_TABLE_KW);

        this.path = path;
        reader = new BufferedReader(new FileReader(path));
    }

    private void scanFile(String path) throws IOException {
        StringBuffer CommonPatternBuf = new StringBuffer();
        for (String name : keywords.keySet())
            CommonPatternBuf.append(String.format("|(?<%s>%s)", name, keywords.get(name)));
        for (String name : terminals.keySet())
            CommonPatternBuf.append(String.format("|(?<%s>%s)", name, terminals.get(name)));
        CommonPattern = Pattern.compile(CommonPatternBuf.substring(1));

        String line;
        while ( (line = reader.readLine()) != null) {
            scanLine(line);
        }
    }

    private void scanLine(String line) {
        Matcher m = CommonPattern.matcher(line);
        boolean keyword_found;
        String value;
        while (m.find()) {
            keyword_found = false;
            for (String name : keywords.keySet()) {
                if ((value = m.group(name)) != null) {
                    tokens.add(new Token(name, value));
                    keyword_found = true;
                    break;
                }
            }
            if (!keyword_found)
                for (String name : terminals.keySet()) {
                    if ((value = m.group(name)) != null) {
                        tokens.add(new Token(name, value));
                        break;
                    }

                }

        }
    }

    public ArrayList<Token> getTokens() throws IOException {
        scanFile(path);
        return tokens;
    }
}
