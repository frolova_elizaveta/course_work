package interpreter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        try {
            //scan tokens
            Lexer lex = new Lexer("check.file");
            ArrayList<Token> tokens = lex.getTokens();
            //print token stream
            for (int i = 0; i < tokens.size(); i++ )
                System.out.println(String.format("%d %s %s", i, tokens.get(i).getName(), tokens.get(i).getValue()));
            Parser parser = new Parser();
            parser.execute(tokens);
        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        } catch (IOException e) {
            System.out.println("Error while reading file.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
