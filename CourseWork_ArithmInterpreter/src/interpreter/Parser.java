package interpreter;

import java.util.ArrayList;
import java.util.ListIterator;

public class Parser {
    private ArrayList<Token> tokens;

    private ListIterator<Token> tokenIterator;

    private PolizProcessor pp;

    public Parser() {
        tokens = new ArrayList<>();
        pp = new PolizProcessor();
    }

    public boolean execute(ArrayList<Token> tokens) throws Exception {
        boolean exist;
        this.tokens = tokens;
        tokenIterator = this.tokens.listIterator();
        exist = lang();
        if (exist)
            return true;
        else
            throw new Exception("Syntax error at token " + tokenIterator.nextIndex());
    }

    private boolean lang() throws Exception {
        boolean exist = false;
        while (tokenIterator.hasNext()) {
            if (lang_unit())
                exist = true;
            else {
                exist = false;
                break;
            }
        }
        return exist;
    }

    private boolean lang_unit() throws Exception {
        int token_index = tokenIterator.nextIndex();
        if (do_assign())
            return true;
        else {
            tokenIterator = tokens.listIterator(token_index);
            if (do_declare())
                return true;
            else {
                tokenIterator = tokens.listIterator(token_index);
                if (do_if_unit())
                    return true;
                else {
                    tokenIterator = tokens.listIterator(token_index);
                    if (do_while_cycle())
                        return true;
                    else {
                        tokenIterator = tokens.listIterator(token_index);
                        if (do_dowhile_cycle())
                            return true;
                        else {
                            tokenIterator = tokens.listIterator(token_index);
                            if (do_for_cycle())
                                return true;
                            else {
                                tokenIterator = tokens.listIterator(token_index);
                                if (do_write())
                                    return true;
                                else {
                                    tokenIterator = tokens.listIterator(token_index);
                                    if (write_table())
                                        return true;
                                    else
                                        return false;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean do_assign() throws Exception {
        Token assigned_var, token;
        ArrayList<Token> token_acc = new ArrayList<>();
        int token_index, expr_value;
        if ((assigned_var = tokenIterator.next()).getName().equals("VAR") && tokenIterator.next().getName().equals("ASSIGNOP")) {
            token_index = tokenIterator.nextIndex();
            if (expr() && tokenIterator.next().getName().equals("SM")) {
                tokenIterator = tokens.listIterator(token_index);
                while (!(token = tokenIterator.next()).getName().equals("SM"))
                    token_acc.add(token);
                token_acc.add(token);
                expr_value = pp.calculateExpr(token_acc);
                pp.setVarValue(assigned_var.getValue(), expr_value);
                return true;
            }
        }
        return false;
    }

    private boolean expr() {
        boolean exist = false;
        if (expr_unit()) {
            exist = true;
            while (tokenIterator.next().getName().equals("OP"))
                if (!expr_unit())
                    exist = false;
            tokenIterator.previous();
        }
        return exist;
    }

    private boolean expr_unit() {
        int token_index = tokenIterator.nextIndex();
        if (operand())
            return true;
        else {
            tokenIterator = tokens.listIterator(token_index);
            if (tokenIterator.next().getName().equals("LPAR") && expr() && tokenIterator.next().getName().equals("RPAR"))
                return true;
        }
        return false;
    }

    private boolean operand() {
        Token token = tokenIterator.next();
        if (token.getName().equals("VAR") || token.getName().equals("NUM"))
            return true;
        else
            return false;
    }

    private boolean do_declare() {
        Token var;
        if (tokenIterator.next().getName().equals("VARKW") && (var = tokenIterator.next()).getName().equals("VAR") &&
                tokenIterator.next().getName().equals("SM")) {
            pp.addVar(var.getValue());
            return true;
        } else
            return false;
    }

    private boolean do_write() {
        Token var;
        if (tokenIterator.next().getName().equals("WRITEKW") && (var = tokenIterator.next()).getName().equals("VAR") &&
                tokenIterator.next().getName().equals("SM")) {
            System.out.println(var.getValue() + " = " + pp.getVarValue(var.getValue()));
            return true;
        } else
            return false;
    }

    private boolean do_if_unit() throws Exception {
        int token_index, cond_value;
        Token token;
        ArrayList<Token> token_acc = new ArrayList<>();
        if (tokenIterator.next().getName().equals("IFKW") && tokenIterator.next().getName().equals("LPAR")) {
            token_index = tokenIterator.nextIndex();
            if (expr() && tokenIterator.next().getName().equals("RPAR")) {
                tokenIterator = tokens.listIterator(token_index);
                while (!(token = tokenIterator.next()).getName().equals("RPAR"))
                    token_acc.add(token);
                token_acc.add(new Token("SM", ";"));
                cond_value = pp.calculateExpr(token_acc);
                if (cond_value != 0) {
                    if (do_if_branch())
                        return true;
                } else {
                    if (do_else_branch())
                        return true;
                }
            }
        }
        return false;
    }

    private boolean do_if_branch() throws Exception {
        boolean exist;
        if (tokenIterator.next().getName().equals("BEGINKW")) {
            exist = true;
            while (!tokenIterator.next().getName().equals("ENDKW")) {
                exist = false;
                tokenIterator.previous();
                if (lang_unit())
                    exist = true;
            }
            if (!exist)
                return false;
            if (tokenIterator.next().getName().equals("ELSEKW")) {
                skip_to_end();
                return true;
            } else {
                tokenIterator.previous();
                return true;
            }
        }
        return false;
    }

    private boolean do_else_branch() throws Exception {
        boolean exist;
        if (skip_to_end()) {
            if (tokenIterator.next().getName().equals("ELSEKW")) {
                if (tokenIterator.next().getName().equals("BEGINKW")) {
                    exist = true;
                    while (!tokenIterator.next().getName().equals("ENDKW")) {
                        exist = false;
                        tokenIterator.previous();
                        if (lang_unit())
                            exist = true;
                    }
                    return exist;
                } else
                    return false;
            } else {
                tokenIterator.previous();
                return true;
            }
        }
        return false;
    }

    private boolean skip_to_end() {
        int nest = 0;
        if (tokenIterator.next().getName().equals("BEGINKW")) {
            nest++;
            while (nest > 0) {
                Token token = tokenIterator.next();
                if (token.getName().equals("BEGINKW"))
                    nest++;
                else if (token.getName().equals("ENDKW"))
                    nest--;
            }
            return true;
        } else
            return false;
    }

    private boolean do_while_cycle() throws Exception {
        int token_index, start_index;
        Token token;
        ArrayList<Token> token_acc = new ArrayList<>();
        if (tokenIterator.next().getName().equals("WHILEKW") && tokenIterator.next().getName().equals("LPAR")) {
            token_index = tokenIterator.nextIndex();
            if (expr() && tokenIterator.next().getName().equals("RPAR")) {
                start_index = tokenIterator.nextIndex();
                tokenIterator = tokens.listIterator(token_index);
                while (!(token = tokenIterator.next()).getName().equals("RPAR"))
                    token_acc.add(token);
                token_acc.add(new Token("SM", ";"));
                while (pp.calculateExpr(token_acc) != 0) {
                    tokenIterator = tokens.listIterator(start_index);
                    if (!do_cycle_body())
                        return false;
                }
                return true;
            }
            else
                return false;
        }
        return false;
    }

    private boolean do_cycle_body() throws Exception {
        boolean exist;
        if (tokenIterator.next().getName().equals("BEGINKW")) {
            exist = true;
            while (!tokenIterator.next().getName().equals("ENDKW")) {
                exist = false;
                tokenIterator.previous();
                if (lang_unit())
                    exist = true;
            }
            return exist;
        }
        return false;
    }

    private boolean do_dowhile_cycle() throws Exception {
        int start_index, token_index, end_index;
        Token token;
        ArrayList<Token> token_acc = new ArrayList<>();
        if (tokenIterator.next().getName().equals("DOKW")) {
            start_index = tokenIterator.nextIndex();
            if (!do_cycle_body())
                return false;

            if (tokenIterator.next().getName().equals("WHILEKW") && tokenIterator.next().getName().equals("LPAR")) {
                token_index = tokenIterator.nextIndex();
                if (expr() && tokenIterator.next().getName().equals("RPAR")) {
                    end_index = tokenIterator.nextIndex();
                    tokenIterator = tokens.listIterator(token_index);
                    while (!(token = tokenIterator.next()).getName().equals("RPAR"))
                        token_acc.add(token);
                    token_acc.add(new Token("SM", ";"));
                }
                else
                    return false;
            }
            else
                return false;
            while (pp.calculateExpr(token_acc) != 0) {
                tokenIterator = tokens.listIterator(start_index);
                if (!do_cycle_body())
                    return false;
            }
            tokenIterator = tokens.listIterator(end_index);
            return true;
        }
        return false;
    }

    private boolean do_for_cycle() throws Exception {
        int token_index, start_index, var_value;
        Token token, var;
        ArrayList<Token> token_acc_cond = new ArrayList<>();
        ArrayList<Token> token_acc_action = new ArrayList<>();
        if (tokenIterator.next().getName().equals("FORKW") && tokenIterator.next().getName().equals("LPAR") && do_assign()) {
            token_index = tokenIterator.nextIndex();
            if (expr() && tokenIterator.next().getName().equals("SM")) {
                tokenIterator = tokens.listIterator(token_index);
                while (!(token = tokenIterator.next()).getName().equals("SM"))
                    token_acc_cond.add(token);
                token_acc_cond.add(token);
            } else
                return false;

            if ((var = tokenIterator.next()).getName().equals("VAR") && tokenIterator.next().getName().equals("ASSIGNOP")) {
                token_index = tokenIterator.nextIndex();
                if (expr() && tokenIterator.next().getName().equals("RPAR")) {
                    tokenIterator = tokens.listIterator(token_index);
                    while (!(token = tokenIterator.next()).getName().equals("RPAR"))
                        token_acc_action.add(token);
                    token_acc_action.add(new Token("SM", ";"));
                } else
                    return false;
            } else
                return false;

            start_index = tokenIterator.nextIndex();
            while (pp.calculateExpr(token_acc_cond) != 0) {
                tokenIterator = tokens.listIterator(start_index);
                if (!do_cycle_body())
                    return false;
                var_value = pp.calculateExpr(token_acc_action);
                pp.setVarValue(var.getValue(), var_value);
            }
            return true;
        }
        return false;
    }

    private boolean write_table() {
        if (tokenIterator.next().getName().equals("WRITETABLEKW") && tokenIterator.next().getName().equals("SM")) {
            pp.printVarTable();
            return true;
        }
        else
            return false;
    }
}
