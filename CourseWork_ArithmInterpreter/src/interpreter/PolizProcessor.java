package interpreter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

public class PolizProcessor {

    private HashMap<String, Integer> varTable;

    private ArrayList<Token> tokens;
    private ArrayList<Token> poliz_tokens;

    private Stack<Token> tokenStack;
    private Stack<Integer> numberStack;

    private HashMap<String, Integer> priority;

    public PolizProcessor() {
        varTable = new HashMap<>();
        tokens = new ArrayList<>();
        poliz_tokens = new ArrayList<>();
        tokenStack = new Stack<>();
        numberStack = new Stack<>();
        priority = new HashMap<>();

        priority.put("*", 1);
        priority.put("/", 1);
        priority.put("+", 2);
        priority.put("-", 2);
        priority.put(">=", 3);
        priority.put("<=", 3);
        priority.put(">", 3);
        priority.put("<", 3);
        priority.put("==", 4);
        priority.put("<>", 4);
        priority.put("&", 5);
        priority.put("|", 6);
    }

    public void addVar(String var_name) {
        varTable.put(var_name, 0);
    }

    public int getVarValue(String var_name) {
        return varTable.get(var_name);
    }

    public void setVarValue(String name, int value) {
        varTable.put(name, value);
    }

    public int calculateExpr(ArrayList<Token> tokens) throws Exception{
        this.tokens = tokens;
        tokenStack.clear();
        numberStack.clear();
        getPoliz();
        calculatePoliz();
        return numberStack.pop();
    }

    public void printVarTable() {
        System.out.println("Variable table:");
        for (String var : varTable.keySet())
            System.out.println("Var " + var + ": " + varTable.get(var));
    }

    private void getPoliz() {
        for (int i = 0; i < tokens.size(); i++)
            dijkstra(tokens.get(i));
    }

    private void dijkstra(Token currentToken) {
        String t_name, t_value;
        switch (currentToken.getName()) {
            case "VAR":
                poliz_tokens.add(currentToken);
                break;
            case "NUM":
                poliz_tokens.add(currentToken);
                break;
            case "OP":
                if (!tokenStack.empty()) {
                    t_name = tokenStack.peek().getName();
                    t_value = tokenStack.peek().getValue();
                    while ((t_name  == "OP") ) {
                        if (priority.get(currentToken.getValue()) >= priority.get(t_value))
                            poliz_tokens.add(tokenStack.pop());
                        else
                            break;
                        if (!tokenStack.empty()) {
                            t_name = tokenStack.peek().getName();
                            t_value = tokenStack.peek().getValue();
                        }
                        else
                            break;
                    }
                }
                tokenStack.add(currentToken);
                break;
            case "LPAR":
                tokenStack.add(currentToken);
                break;
            case "RPAR":
                while(tokenStack.peek().getName() != "LPAR")
                    poliz_tokens.add(tokenStack.pop());
                tokenStack.pop();
                break;
            case "SM":
                while(!tokenStack.empty())
                    poliz_tokens.add(tokenStack.pop());
            default:
                break;

        }
    }

    private void calculatePoliz() throws Exception{
        for (int i = 0; i < poliz_tokens.size(); i++) {
            processToken(poliz_tokens.get(i));
        }
    }

    private void processToken(Token currentToken) throws Exception {
        switch (currentToken.getName()) {
            case "NUM":
                numberStack.push(Integer.parseInt(currentToken.getValue()));
                break;
            case "VAR":
                if (varTable.containsKey(currentToken.getValue()))
                    numberStack.push(varTable.get(currentToken.getValue()));
                else
                    throw new Exception("Unidentified variable: " + currentToken.getValue());
                break;
            case "OP":
                processOp(currentToken.getValue());
                break;
        }
    }

    private void processOp(String op) {
        int op1, op2;
        switch (op) {
            case "+" :
                op2 = numberStack.pop();
                op1 = numberStack.pop();
                numberStack.push(op1 + op2);
                break;

            case "-" :
                op2 = numberStack.pop();
                op1 = numberStack.pop();
                numberStack.push(op1 - op2);
                break;
            case "*" :
                op2 = numberStack.pop();
                op1 = numberStack.pop();
                numberStack.push(op1 * op2);
                break;
            case "/" :
                op2 = numberStack.pop();
                op1 = numberStack.pop();
                numberStack.push(op1 / op2);
                break;
            case "==" :
                op2 = numberStack.pop();
                op1 = numberStack.pop();
                if (op1 == op2)
                    numberStack.push(1);
                else
                    numberStack.push(0);
                break;

            case "<>" :
                op2 = numberStack.pop();
                op1 = numberStack.pop();
                if (op1 != op2)
                    numberStack.push(1);
                else
                    numberStack.push(0);
                break;

            case ">=" :
                op2 = numberStack.pop();
                op1 = numberStack.pop();
                if (op1 >= op2)
                    numberStack.push(1);
                else
                    numberStack.push(0);
                break;

            case ">" :
                op2 = numberStack.pop();
                op1 = numberStack.pop();
                if (op1 > op2)
                    numberStack.push(1);
                else
                    numberStack.push(0);
                break;

            case "<=" :
                op2 = numberStack.pop();
                op1 = numberStack.pop();
                if (op1 <= op2)
                    numberStack.push(1);
                else
                    numberStack.push(0);
                break;

            case "<" :
                op2 = numberStack.pop();
                op1 = numberStack.pop();
                if (op1 < op2)
                    numberStack.push(1);
                else
                    numberStack.push(0);
                break;

            case "&" :
                op2 = numberStack.pop();
                op1 = numberStack.pop();
                if (op1 != 0  &&  op2 != 0)
                    numberStack.push(1);
                else
                    numberStack.push(0);
                break;

            case "|" :
                op2 = numberStack.pop();
                op1 = numberStack.pop();
                if (op1 != 0  ||  op2 != 0)
                    numberStack.push(1);
                else
                    numberStack.push(0);
                break;

        }
    }

}
